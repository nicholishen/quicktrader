//+------------------------------------------------------------------+
//|                                                  QuickTrader.mqh |
//|                                                      nicholishen |
//|                                         nicholishen@tutanota.com |
//+------------------------------------------------------------------+
#property copyright "nicholishen"
#property link      "nicholishen@tutanota.com"
#property version   "1.00"
#property strict

enum ButtonMode{
   B_BUY,
   B_SELL,
   B_MARKET,
   B_BUY_LIMIT,
   B_SELL_LIMIT,
   B_BUY_STOP,
   B_SELL_STOP,
   B_NULL
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class CButton
{
 protected:
   string      m_ID;
   string      m_text;
   int         m_xDist;
   int         m_yDist;
   int         m_xSize;
   int         m_ySize;
   ButtonMode  m_mode;
   int         m_fontSize;

 public:
                     CButton( string      ID,
                              string      text,
                              int         xDist,
                              int         yDist,
                              int         xSize,
                              int         ySize,
                              int         fontSize,
                              ButtonMode  mode 
                            );
                    ~CButton();
   string            getID(){return m_ID;}
   string            getText(){return m_text;}
   void              setText(string text){m_text = text;}                 
   void              create();
   void              remove();
   void              swap(string text, ButtonMode mode);
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
CButton::swap(string text,ButtonMode mode){
   m_text = text;
   m_mode = mode; 
   ObjectSetString(0,m_ID,OBJPROP_TEXT,m_text);
   return;
}

CButton::CButton(string ID,
          string        text,
          int           xDist,
          int           yDist,
          int           xSize,
          int           ySize,
          int           fontSize,
          ButtonMode    mode 
         ):
         m_ID(ID),
         m_text(text),
         m_xDist(xDist),
         m_yDist(yDist),
         m_xSize(xSize),
         m_ySize(ySize),
         m_fontSize(fontSize),
         m_mode(mode)
  {
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
CButton::~CButton()
  {
  }
//+------------------------------------------------------------------+



void CButton::create(){
   ObjectCreate      (0,m_ID,OBJ_BUTTON,0,100,100);
   ObjectSetInteger  (0,m_ID,OBJPROP_COLOR,clrWhite);
   ObjectSetInteger  (0,m_ID,OBJPROP_BGCOLOR,clrGray);
   ObjectSetInteger  (0,m_ID,OBJPROP_XDISTANCE,m_xDist);
   ObjectSetInteger  (0,m_ID,OBJPROP_YDISTANCE,m_yDist);
   ObjectSetInteger  (0,m_ID,OBJPROP_XSIZE,m_xSize);
   ObjectSetInteger  (0,m_ID,OBJPROP_YSIZE,m_ySize);
   ObjectSetString   (0,m_ID,OBJPROP_FONT,"Arial");
   ObjectSetString   (0,m_ID,OBJPROP_TEXT,m_text);
   ObjectSetInteger  (0,m_ID,OBJPROP_FONTSIZE,m_fontSize);
   ObjectSetInteger  (0,m_ID,OBJPROP_SELECTABLE,0);
}
void CButton::remove(void){
   ObjectDelete(0,m_ID);
}


//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------

class CLabel {
   protected:
      string      m_ID;
      string      m_text;
      int         m_xDist;
      int         m_yDist;
      int         m_fontSize;
      color       m_color;

   public:
   
                     CLabel(  string      ID,
                              string      text,
                              int         xDist,
                              int         yDist,
                              int         fontSize,
                              color       col                    
                           );
                     ~CLabel();
   void              swap(string text);
   void              create();
   void              remove();
};

CLabel::CLabel(string ID,
               string text,
               int xDist,
               int yDist,
               int fontSize, 
               color col      
               ):
               m_ID(ID),
               m_text(text),
               m_xDist(xDist),
               m_yDist(yDist),
               m_fontSize(fontSize),
               m_color(col)

  {
  }
CLabel::~CLabel(void){}
//+-

void CLabel::create(void){
   ObjectCreate    (0,m_ID,OBJ_LABEL,0,100,100);
   ObjectSetInteger(0,m_ID,OBJPROP_COLOR,m_color);
   ObjectSetInteger(0,m_ID,OBJPROP_XDISTANCE,m_xDist);
   ObjectSetInteger(0,m_ID,OBJPROP_YDISTANCE,m_yDist);
   ObjectSetString (0,m_ID,OBJPROP_FONT,"Trebuchet MS");
   ObjectSetString (0,m_ID,OBJPROP_TEXT,m_text);
   ObjectSetInteger(0,m_ID,OBJPROP_FONTSIZE,m_fontSize);
   ObjectSetInteger(0,m_ID,OBJPROP_SELECTABLE,0);
}

void CLabel::swap(string text){
   m_text = text;
   ObjectSetString (0,m_ID,OBJPROP_TEXT,m_text);
}

void CLabel::remove(void){
   ObjectDelete(0,m_ID);
}