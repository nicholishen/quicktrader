//+------------------------------------------------------------------+
//|                                                  QuickTrader.mq4 |
//|                                                      nicholishen |
//|                                         nicholishen@tutanota.com |
//+------------------------------------------------------------------+
#property copyright "nicholishen"
#property link      "www.reddit.com/u/nicholishenFX"
#property version   "1.01"
#property strict
#include "QuickTrader.mqh"
#include <stdlib.mqh>
#include <StdLibErr.mqh>

enum BUTTON_MODE {
  STEP_START_MODE,
  STEP_ORDER_TYPE_DECISION,
  STEP_FLOAT_MARKET,
  STEP_ACTIVE
};

enum LINETYPE{
   ENTRY,
   SL,
   TP
};

struct ObjProps {
   string      name;
   double      value;
   string      objText;
   long        style;
   color       clr;
   int         width;
   ButtonMode  mode;
   ENUM_ORDER_TYPE  trade;
   string info;
   
   double      pipsFrom;
   double lots;
   double maxGain;
   double maxLoss;
   LINETYPE lineType;
   ObjProps(LINETYPE t):lineType(t){}
};

input int AccountRiskPercent        = 1;
input int DefaultLimitEntryPips     = 20;
input int StopLossPips              = 200;
input int TakeProfitPips            = 200;
input int Slippage                  = 0;
input int MagicNumber               = 37686;

string b1ID = "Button1";
string b2ID = "Button2";
string b3ID = "Button3";
string b4ID = "Button4";

bool isFloat    = false;
double maxLoss = 0;
double maxGain = 0;

ObjProps stopLoss(SL);
ObjProps takeProfit(TP);
ObjProps entry(ENTRY);

BUTTON_MODE buttonStep;
string labelID="Info";
string label2ID="Info2";
string label3ID="Info3";

CButton b1(b1ID,"BUY", 75,50,70,20,8,B_BUY);
CButton b2(b2ID,"SELL",150,50,70,20,8,B_SELL);
CButton b3(b3ID,"BACK",0,50,70,20,8,B_NULL);
CButton b4(b4ID,"$ CLOSE!",0,20,70,20,8,B_NULL);

CLabel  label(labelID,"Select trade setup",230,30,12,clrDeepSkyBlue);
CLabel  label2(label2ID,"Select trade setup",230,50,12,clrDeepSkyBlue);
CLabel  label3(label3ID,"POSITION INFO",230,0,12,clrRed);

//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
{
   Comment("");
   if(!IsDemo()){
      if(
         MessageBox
            ("This is untested software. Are you sure you want to proceed on a live account?",
            NULL,
            MB_YESNO 
            )
            == IDNO
         )
      {
         return INIT_FAILED;
      }
      
      //return INIT_FAILED;
   }
   //Comment("");
   if(!ChartEventObjectCreateSet(true)){
      Print(__FUNCTION__," Error: Failed to set flag to watch object events.");
      return INIT_FAILED;      
   }
   
   buttonStep = STEP_START_MODE; // set initial step
   
   b1.create();
   b2.create();
   b3.create();
   if(orderCnt()> 0) b4.create();
   label.create();
   label3.create();
   
   stopLoss.name        = "nn.stoploss";
   stopLoss.objText     = "*STOP LOSS*";
   stopLoss.style       = STYLE_DOT;
   stopLoss.clr         = clrYellow;
   stopLoss.width       = 5;
   
   takeProfit.name      = "nn.takeprofit";
   takeProfit.objText   = "*TAKE PROFIT*";
   takeProfit.style     = STYLE_DOT;
   takeProfit.clr       = clrYellow;
   takeProfit.width     = 5;
   
   entry.name           = "nn.entry";
   entry.objText        = "*ENTRY*";
   entry.style          = STYLE_DOT;
   entry.clr            = clrYellowGreen;
   entry.width          = 5;
   
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---
   b1.remove();
   b2.remove();
   b3.remove();
   label.remove();
   label2.remove();
   label3.remove();
   ObjectsDeleteAll(0,"nn");
   Comment("");
   
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
//---
   // code block to alter the order type when price crosses the unconfirmed entry.
   if(buttonStep == STEP_ORDER_TYPE_DECISION){
      if(entry.value > Bid && entry.trade == OP_BUYLIMIT){
         entry.trade = OP_BUYSTOP;
         entry.objText = "*BUY STOP*";
         drawLine(entry);
      }
      if(entry.value < Bid && entry.trade == OP_BUYSTOP){
         entry.trade = OP_BUYLIMIT;
         entry.objText = "*BUY LIMIT*";
         drawLine(entry);
      }
      if(entry.value < Bid && entry.trade == OP_SELLLIMIT){
         entry.trade = OP_SELLSTOP;
         entry.objText = "*SELL STOP*";
         drawLine(entry);
      }
      if(entry.value > Bid && entry.trade == OP_SELLSTOP){
         entry.trade = OP_SELLLIMIT;
         entry.objText = "*SELL LIMIT*";
         drawLine(entry);
      }
      
   }
   if(buttonStep == STEP_FLOAT_MARKET){
      if(isFloat){
         track(entry.mode);
         
      }
   }
}

/*void makeComment(){
   string header = "\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r";
   string sep = "------------------------------------------------------------------------\n\r";
   string risk = StringConcatenate("Account risk set to ",
                                    IntegerToString(AccountRiskPercent),
                                    "% for a potential loss of $",
                                    DoubleToString(maxLoss,2)
                                  );
   
   Comment(header+sep+risk);
}*/
void track(ButtonMode mode){

            if(mode == B_BUY){
               entry.value       = Ask;
               takeProfit.value  = Ask + takeProfit.pipsFrom;
               stopLoss.value    = Ask - stopLoss.pipsFrom;
               drawLine(stopLoss);
               drawLine(takeProfit);
               drawLine(entry);
               
               
            }
            else if(mode == B_SELL){
               entry.value       = Bid;
               takeProfit.value  = Bid - takeProfit.pipsFrom;
               stopLoss.value    = Bid + stopLoss.pipsFrom;
               drawLine(stopLoss);
               drawLine(takeProfit);
               drawLine(entry);
               
            }
}
            
void buttonSet(BUTTON_MODE mode){ 
   switch(mode){
      case STEP_ORDER_TYPE_DECISION: 
            buttonStep = STEP_ORDER_TYPE_DECISION;  
            b1.swap("$ PENDING",B_NULL);
            b1.remove();
            b1.create();
            b2.swap("FLOAT",B_NULL);
            b2.remove();
            b2.create();
            b3.swap("BACK",B_NULL);
            b3.remove();
            b3.create();
            b4.remove();
            label.swap("Click and drag to adjust trade levels.");
            label2.swap("'FLOAT' locks the stop levels to the current price.");
            label2.create();
            if(entry.mode==B_BUY){   
               entry.trade = OP_BUYLIMIT;
               entry.objText = "*BUY LIMIT*";
            }
            if(entry.mode==B_SELL){  
               entry.trade = OP_SELLLIMIT;
               entry.objText = "*SELL LIMIT*";
            }
            entry.width = 4;
            takeProfit.width = 4;
            stopLoss.width = 4;
            drawLine(entry);
            drawLine(takeProfit);
            drawLine(stopLoss);
            entry.lots = calcLots();
            return;
       case STEP_START_MODE:
            b1.swap("BUY",B_NULL);
            b1.remove();
            b1.create();
            b2.swap("SELL",B_NULL);
            b2.remove();
            b2.create();
            if(orderCnt()>0)b4.create();
            label2.remove();
            label.swap("Select trade setup");
            b3.swap("BACK",B_NULL);
            b3.remove();
            b3.create();
            buttonStep = STEP_START_MODE;
            takeProfit.width = 4;
            stopLoss.width = 4;
            entry.width =4;
            ObjectsDeleteAll(0,"nn");
            return;
       case STEP_FLOAT_MARKET:
            buttonStep = STEP_FLOAT_MARKET;
              
            isFloat = true; 
            b1.remove();
            b2.remove();
            label.swap("Stop levels are now locked to current price.");
            label2.swap("Market order ready to execute!");
            b1.swap("$ MARKET",B_MARKET);
            b1.create();
            b2.swap("LOCK",B_NULL);
            b2.remove(); // removing lock in favor of back button
            b3.swap("BACK",B_NULL);
            b3.remove();
            b3.create();
            entry.objText = "*MARKET ORDER*";
            entry.width = 1;
            takeProfit.width =1;
            stopLoss.width = 1;
            track(entry.mode);
            entry.lots = calcLots();
            return;
       case STEP_ACTIVE:
            buttonStep = STEP_ACTIVE;
            b1.swap("$ CLOSE!",B_NULL);
            b1.remove();
            b1.create();
            b2.remove();
            b3.swap("REMOVE EA",B_NULL);
            b3.remove();
            b3.create();
            label2.remove();
            label.swap("<------ CLOSE ALL ORDERS GENERATED BY THIS APP");
            ObjectsDeleteAll(0,"nn");
            return;
   }
}
//+------------------------------------------------------------------+
//| ChartEvent function                                              |
//+------------------------------------------------------------------+
void OnChartEvent(
                  const int id,
                  const long &lparam,
                  const double &dparam,
                  const string &sparam
                 )
{

   /*if( id == CHARTEVENT_CHART_CHANGE){
      buttonSet(STEP_START_MODE);
   }*/
   
   RefreshRates();
   
   if(id == CHARTEVENT_OBJECT_CLICK){
      if(buttonStep == STEP_ACTIVE){ // remove EA
         if(sparam == b3ID){
            if(MessageBox("Are you sure you'd like to quit?",NULL,MB_YESNO) == IDYES){   
               ExpertRemove();
            }else{
               b3.remove();
               b3.create();
               return;
            }               
         }
         if(sparam == b1ID){ // Close all orders
            closeAllOrders();
            buttonSet(STEP_START_MODE);
            return;
         }         
      }
      if(buttonStep == STEP_FLOAT_MARKET){
         if(sparam == b3ID){ // Back here pressed
            isFloat = false;
            buttonStep = STEP_ORDER_TYPE_DECISION;
            buttonSet(buttonStep);
            return;
         }
         if(sparam == b1ID){ // initiate instant market order
            if(entry.mode == B_BUY){
               entry.trade = OP_BUY;
            }else if (entry.mode == B_SELL){
               entry.trade = OP_SELL;
            }
            if(order()){
               buttonSet(STEP_ACTIVE);
               return;
            }else{
               b1.remove();
               b1.create();
               return;
            }
         }
      }   
      if(buttonStep == STEP_START_MODE){
         if(sparam == b4ID){ // Close all orders
            closeAllOrders();
            buttonSet(STEP_START_MODE);
            b4.remove();
            return;
         } 
         if(sparam == b3ID){ // Back button to exit EA
            if(MessageBox("Are you sure you'd like to quit?",NULL,MB_YESNO) == IDYES){   
               ExpertRemove();
            }else{
               b3.remove();
               b3.create();
               return;
            }               
         }
         bool set = false;
         if(sparam== b1ID){ //buy
            entry.value          = NormalizeDouble(Ask - DefaultLimitEntryPips * _Point,_Digits); 
            entry.mode           = B_BUY;
            entry.trade          = OP_BUYLIMIT;
            entry.objText        = "*BUY LIMIT*";
            stopLoss.value       = NormalizeDouble(entry.value - StopLossPips * _Point,_Digits);
            takeProfit.value     = NormalizeDouble(entry.value + TakeProfitPips * _Point,_Digits);
            takeProfit.pipsFrom  = takeProfit.value - entry.value;
            stopLoss.pipsFrom    = entry.value - stopLoss.value;
            entry.lots = calcLots();
            drawLine(stopLoss);
            drawLine(takeProfit);
            drawLine(entry);
            set = true;
         }
         else if(sparam== b2ID){ //sell
            entry.value          = Bid + DefaultLimitEntryPips * _Point;
            entry.mode           = B_SELL;
            entry.trade          = OP_SELLLIMIT;
            entry.objText        = "*SELL LIMIT*";
            stopLoss.value       = entry.value + StopLossPips * _Point;
            takeProfit.value     = entry.value - TakeProfitPips * _Point;
            takeProfit.pipsFrom  = entry.value - takeProfit.value;
            stopLoss.pipsFrom    = stopLoss.value - entry.value;
            entry.lots = calcLots();
            drawLine(stopLoss);
            drawLine(takeProfit);
            drawLine(entry);
            set = true;    
         }
         if(set){
            buttonSet(STEP_ORDER_TYPE_DECISION);
            
            return;
         }
      }
      if(buttonStep == STEP_ORDER_TYPE_DECISION /* name to something better*/){
         if(sparam == b1ID){
            // place pending orders
            if(!order()){
               Print(__FUNCTION__,"ORDER ERROR");
               buttonSet(STEP_ORDER_TYPE_DECISION);
               return;
            } else {
               buttonSet(STEP_ACTIVE);
               return;
            }
         }
         if(sparam == b2ID){
            // float
            buttonSet(STEP_FLOAT_MARKET);
            
            return;
            
         }
         if(sparam == b3ID){
            // back button
            buttonSet(STEP_START_MODE);
            return;
         }
      }
   }
   // end click event
   if(id == CHARTEVENT_OBJECT_DRAG){ 
      if(buttonStep == STEP_ORDER_TYPE_DECISION){
         if( sparam == entry.name){
            entry.value = NormalizeDouble(ObjectGetDouble(0,sparam,OBJPROP_PRICE),_Digits);
            if(entry.mode == B_BUY){
               if(entry.value > Bid){
                  entry.trade = OP_BUYSTOP;
                  entry.objText = "*BUY STOP*";
               } else {
                  entry.trade = OP_BUYLIMIT;
                  entry.objText = "*BUY LIMIT*";
               }
               takeProfit.pipsFrom = takeProfit.value - entry.value;
               stopLoss.pipsFrom = entry.value - stopLoss.value;
            }
            else if(entry.mode == B_SELL){
               if(entry.value < Bid){
                  entry.trade = OP_SELLSTOP;
                  entry.objText = "*SELL STOP*";
               } else {
                  entry.trade = OP_SELLLIMIT;
                  entry.objText = "*SELL LIMIT*";
               }
               takeProfit.pipsFrom = entry.value - takeProfit.value;
               stopLoss.pipsFrom = stopLoss.value - entry.value;
            }
            
            
            entry.lots = calcLots();
            drawLine(takeProfit);
            drawLine(stopLoss);
            drawLine(entry);
         }
         if( sparam == stopLoss.name){
            stopLoss.value = NormalizeDouble(ObjectGetDouble(0,sparam,OBJPROP_PRICE),_Digits);
            
            if(entry.mode == B_SELL){
               takeProfit.pipsFrom = entry.value - takeProfit.value;
               stopLoss.pipsFrom = stopLoss.value - entry.value;
            } else
            if(entry.mode == B_BUY){
               takeProfit.pipsFrom = takeProfit.value - entry.value;
               stopLoss.pipsFrom = entry.value - stopLoss.value;
            }
           
            entry.lots = calcLots();
            drawLine(takeProfit);
            drawLine(stopLoss);
            drawLine(entry);
         }
         if( sparam == takeProfit.name){
            takeProfit.value = NormalizeDouble(ObjectGetDouble(0,sparam,OBJPROP_PRICE),_Digits);
            
            if(entry.mode == B_SELL){
               takeProfit.pipsFrom = entry.value - takeProfit.value;
               stopLoss.pipsFrom = stopLoss.value - entry.value;
            } else
            if(entry.mode == B_BUY){
               takeProfit.pipsFrom = takeProfit.value - entry.value;
               stopLoss.pipsFrom = entry.value - stopLoss.value;
            }
            entry.lots = calcLots();
            drawLine(takeProfit);
            drawLine(stopLoss);
            drawLine(entry);
            
         }  
      }
   }
}


bool order(){
   double price = 0;
   if(entry.trade == OP_BUY){
      price = Ask;
   }
   else if(entry.trade == OP_SELL){
      price = Bid;
   }
   
   else if(entry.trade == OP_BUYLIMIT ||
      entry.trade == OP_BUYSTOP ||
      entry.trade == OP_SELLLIMIT ||
      entry.trade == OP_SELLSTOP 
      )
   {
      price = entry.value;
   }
   
   
   if(OrderSend(Symbol(),
               entry.trade,
               entry.lots,
               price,
               Slippage,
               stopLoss.value,
               takeProfit.value,
               "InteractiveQuickTrader",
               MagicNumber
               ) > 0
      )
   {
      return true;
   }
   Print(__FUNCTION__," ",ErrorDescription(GetLastError()));
   return false;
}

void closeAllOrders(){
   RefreshRates();
   int cnt = OrdersTotal();
   while(orderCnt()> 0 ){
         selectNextOrder();
         if(OrderType() == OP_BUY){
            while(!OrderClose(OrderTicket(),OrderLots(),Bid,3)){
               Print(__FUNCTION__,ErrorDescription(GetLastError()));
               Sleep(1000);
               RefreshRates();
            }
         }
         else if(OrderType() == OP_SELL){
            while(!OrderClose(OrderTicket(),OrderLots(),Ask,3)){
               Print(__FUNCTION__,ErrorDescription(GetLastError()));
               Sleep(1000);
               RefreshRates();
            }
         }
         else{
            while(!OrderDelete(OrderTicket())){               
               Print(__FUNCTION__,ErrorDescription(GetLastError()));
               Sleep(1000);
               RefreshRates();
            }
           
         }
      
   }

}

int orderCnt(){
   RefreshRates();
   int orders = 0;
   int cnt = OrdersTotal();
   for( int i = 0; i < cnt; i++){
      bool select = OrderSelect(i,SELECT_BY_POS);
      if(select && OrderSymbol() == Symbol() && OrderMagicNumber() == MagicNumber){
         orders++;
      }
   }
   return orders;
}

bool selectNextOrder(){
   int cnt = OrdersTotal();
   for( int i = 0; i < cnt; i++){
      bool select = OrderSelect(i,SELECT_BY_POS);
      if(select && OrderSymbol() == Symbol() && OrderMagicNumber() == MagicNumber){
         return true;
      }
   }
   return false;
}

      
      
      
void drawLine(ObjProps& props){
   calcLots();
   stopLoss.info = IntegerToString((int)(stopLoss.pipsFrom / _Point)) + " pips. Max Loss = $"+ DoubleToString(entry.maxLoss,2);
   
   takeProfit.info = IntegerToString((int)(takeProfit.pipsFrom / _Point)) + " pips. Max Gain = $"+DoubleToString(entry.maxGain,2);
   entry.info = DoubleToString(entry.lots,2) + " Lots";
   
   ObjectDelete(0,props.name);
   ObjectCreate(0,props.name,OBJ_HLINE,0,TimeCurrent(),props.value);
   ObjectSetInteger(0,props.name,OBJPROP_COLOR,props.clr);
   
   ObjectSetInteger(0,props.name,OBJPROP_WIDTH,props.width);
   ObjectSetInteger(0,props.name,OBJPROP_STYLE,props.style);
   int pip = props.lineType != ENTRY ? (int)(props.pipsFrom / _Point): 0;
   string pipS = pip > 0 ? IntegerToString(pip)+" pips":""; 
   ObjectSetText( props.name,
                        props.objText+" "+
                        //pipS,
                        //DoubleToString(props.value,_Digits) ,
                        props.info,
                     8
                     );
   
}
//+------------------------------------------------------------------+

bool ChartEventObjectCreateSet(const bool value,const long chart_ID=0) 
  { 
//--- reset the error value 
   ResetLastError(); 
//--- set property value 
   if(!ChartSetInteger(chart_ID,CHART_EVENT_OBJECT_CREATE,0,value)) 
     { 
      //--- display the error message in Experts journal 
      Print(__FUNCTION__+", Error Code = ",GetLastError()); 
      return(false); 
     } 
//--- successful execution 
   return(true); 
  }

// TODO  needs refinement for risk mgmt. Now lots will return the minlot even if exceeding risk.
// TODO  need to check to see if stops and tp are place within the range that will be accepted by server. Need to 
// automatically move the levels once they are out of bounds. Also don't place initial levels outside of risk tollerance.
// TODO  make levels survive a chart change by writing to file or global variables.
// TODO Needs to display max gains and losses on level.


double calcLots(){
   if(stopLoss.pipsFrom <= 0) return -1;
   double accountRisk = (double)AccountRiskPercent / 100;
   double balance = AccountBalance();
   double pipValue = MarketInfo(Symbol(),MODE_TICKVALUE);
   double minLot = MarketInfo(Symbol(),MODE_MINLOT);
   double slPoints = stopLoss.pipsFrom / _Point;
   double tpPoints = takeProfit.pipsFrom / _Point;
   double lots = (balance*accountRisk) / (pipValue * slPoints);
   lots = NormalizeDouble(lots,2);
   lots = lots < minLot ? minLot : lots;
   maxLoss = slPoints * pipValue * lots;
   maxGain = tpPoints * pipValue * lots;
   entry.lots = lots;
   entry.maxGain = maxGain;
   entry.maxLoss = maxLoss;
   
   
   //Comment(
   label3.swap(
            DoubleToString(lots,2)+" Lots. Max-Loss = $"+
            DoubleToStr(maxLoss,2)+ " Max-Gain = $"+
            DoubleToStr(maxGain,2)
            
            );
   label3.remove();
   label3.create();
   //Print(DoubleToString(lots,2));
   return lots >= minLot ? lots : -1;
}